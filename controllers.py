import traceback
import asyncio

try:
    header = open('header.html', 'r')
    footer = open('footer.html', 'r')

    body = '<h1> This is the main page</h1>'

    output = header.read()
    output += body
    output += header.read()


except Exception:

    async def logErrors():
        errFile = await open('errors.log', 'a')
        err = traceback.format_exc()
        errFile.write(err)
    
    asyncio.run(logErrors())