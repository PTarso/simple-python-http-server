from http.server import HTTPServer, BaseHTTPRequestHandler
import controllers

class Server(BaseHTTPRequestHandler):
    def do_GET(self):
        
        if self.path == '/':

            # file_to_open = open(self.path[1:]).read()
            self.send_response(200)
            self.end_headers()
            self.wfile.write(bytes(controllers.output, 'utf-8'))


        else:

            file_to_open = "File not found"
            self.send_response(404)
            self.end_headers()
            self.wfile.write(bytes(file_to_open, 'utf-8'))